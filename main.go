package main

import (
	"encoding/csv"
	"flag"
	"fmt"
	"os"
	"path/filepath"
	"strings"

	bf "bardel/files"
	br "bardel/render"
)

const (
	fReg = `^renders_(?P<year>[0-9]{4})-(?P<month>[0-9]{1,2})-(?P<day>[0-9]{1,2}).csv$`
)

////FLAGS
var flagOffset = 0
var flagSet = flag.NewFlagSet("", flag.ExitOnError)

//Filters
var app = flagSet.String("app", ".", "filter output for only renders that use the given application")
var renderer = flagSet.String("renderer", ".", "filter output for only renders that use the given renderer")
var failed = flagSet.Bool("failed", false, "if present, include data from failed renders in the output")

//Bools
var flagBools = map[string]*bool{
	"avgtime": flagSet.Bool("avgtime", false, "output the average render time in seconds"),
	"avgcpu":  flagSet.Bool("avgcpu", false, "output the average peak cpu"),
	"avgram":  flagSet.Bool("avgram", false, "output the average ram usage"),
	"maxram":  flagSet.Bool("maxram", false, "output the id for the render with the highest peak RAM"),
	"maxcpu":  flagSet.Bool("maxcpu", false, "output the id for the render with the highest peak CPU"),
	"summary": flagSet.Bool("summary", false, "output the result of all of the above commands, in the order listed above,each on a separate line"),
}

////END FLAGS

func main() {
	////FILEPATH
	//get our wd
	fp, err := os.Getwd()
	if err != nil {
		panic(err)
	}

	//get an explicitly defined path from cmd if available
	//if the first arg has a negative sign, we can assume it's not a dir
	if len(os.Args) > 1 && !strings.HasPrefix(os.Args[1], "-") && !strings.HasPrefix(os.Args[1], "--") {
		if !filepath.IsAbs(fp) {
			fp = filepath.Join(fp, os.Args[1])
		}

		//test if the path we have is valid, if not we fallback to CWD
		if _, err = os.Stat(fp); os.IsNotExist(err) {
			panic(err)
		}

		flagOffset++
		fp = os.Args[1]
	}

	//normalize the path to absolute always
	fp, err = filepath.Abs(fp)
	if err != nil {
		panic(err)
	}
	////END FILEPATH

	flagSet.Parse(os.Args[1+flagOffset:])

	//Find our files from our filepath and regular expression
	files, err := bf.FilteredInDir(fp, fReg)
	if err != nil {
		panic(err)
	}

	//Iterate through our files
	for _, file := range *files {
		//Obtain a file stream
		fStream, err := file.Open()
		if err != nil {
			panic(err)
		}

		//Create a new Renders{}, this represents our csv collection
		renders := br.NewRenders()

		//Set the Config on incoming flags from stdin
		renders.Config.SetApp(*app)
		renders.Config.SetRenderer(*renderer)
		renders.Config.SetFailed(*failed)

		//Get a new csv reader and pass in our file stream
		//we will be streaming each line individually
		csvR := csv.NewReader(fStream)
		csvR.FieldsPerRecord = -1

		//Iterate through our csv file until we hit EOF
		for {
			r, err := csvR.Read()
			if err != nil {
				if err.Error() == "EOF" {
					break
				}

				panic(err)
			}

			//Add our render, this is our CSV string
			renders.AddRender(r)
		}

		if err := file.Close(); err != nil {
			panic(err)
		}

		//Generate report and send it to stdout
		rr, _ := renders.GenerateReport()
		switch {
		case *flagBools["avgtime"]:
			fmt.Println(rr.Time.Avg)
		case *flagBools["avgcpu"]:
			fmt.Println(rr.CPU.Avg)
		case *flagBools["avgram"]:
			fmt.Println(rr.RAM.Avg)
		case *flagBools["maxram"]:
			fmt.Println(rr.RAM.Max)
		case *flagBools["maxcpu"]:
			fmt.Println(rr.CPU.Max)
		case *flagBools["summary"]:
			fmt.Println(rr.Time.Avg)
			fmt.Println(rr.CPU.Avg)
			fmt.Println(rr.RAM.Avg)
			fmt.Println(rr.RAM.Max)
			fmt.Println(rr.CPU.Max)
		default:
			fmt.Println(rr.TotalReported)
		}
	}
}
