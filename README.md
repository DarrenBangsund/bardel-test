Darren Bangsund

```
bardel-test
│   README.md
│   run.sh
│   makefile
│   main.go    
│
└───src
│   │
│   └───bardel
│       │
│       └───files
|       |   |
|       |   |   files.go
|       |   |   files_test.go
|       |
|       |
│       └───render
│       │   config.go
│       │   render.go
│       │   render_test.go
│       │   report.go
│       │   
```

`main.go` is in the root because the project is designed to be run from the root. The rest of the source files can be found in ./src/bardel/**/*. `main.go` is where most of the library interaction happens. The render parser doesn't care where the data comes from as long as it's in the form of `[]string` so it can, in theory, be used else-where as well. The program will accept an absolute or relative path.

If two of the field flags are included, it will use only the first one. If no files are found, nothing will be passed to stdout.

To build the project, enter the root (the same dir as this readme file) and run `make` to build the project. After that you can run ./run.sh and pass in the necessary arguments.

Note: It's important to define the target CSV file folder as the first argument and include the flags
after. Eg. `./run.sh /path/to/files -avgcpu -failed -app 'appname'` or else the path will not parse properly and just use `$PWD` (I find golang kind of funny with flags sometimes so to save a bunch of dev time and crazy logic I just like to impose some sort of arg structure!)

Tested with `GNU make v4.1` on `Linux Mint 18 Cinnamon 64-bit v3.0.7`