package files

import (
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"testing"
)

const (
	fReg = `^renders_(?P<year>[0-9]{4})-(?P<month>[0-9]{1,2})-(?P<day>[0-9]{1,2}).csv$`
)

func getFiles() (*[]FileHandle, error) {
	fs, _ := os.Getwd()

	return FilteredFilesInDir(filepath.Join(fs, "..", "..", "test-renders"), fReg)
}

func TestGetRendersFilesInDir(t *testing.T) {
	fi, err := getFiles()
	if err != nil {
		t.Error(err)
	}

	log.Printf("Found %v files", len(*fi))
	for _, f := range *fi {
		log.Print(f.Path)
	}
}

func TestOpenFiles(t *testing.T) {
	fi, err := getFiles()
	if err != nil {
		t.Error(err)
	}

	log.Printf("Found %v files", len(*fi))
	for _, f := range *fi {
		r, err := f.Open()
		if err != nil {
			t.Error(err)
		}

		log.Print(ioutil.ReadAll(r))
	}
}
