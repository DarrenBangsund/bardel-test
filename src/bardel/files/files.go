package files

import (
	"io/ioutil"
	"os"
	"path/filepath"
	"regexp"
	"time"
)

//FileHandle is a lightweight handle to a file which has
//convenience methods to open and close the file stream
type FileHandle struct {
	Date time.Time
	Path string
	file *os.File
}

//Open returns a file stream for consumption how
//ever necessary
func (rf *FileHandle) Open() (*os.File, error) {
	fi, err := os.Open(rf.Path)
	if err != nil {
		return nil, err
	}

	rf.file = fi

	return rf.file, nil
}

//Close will close our stream and free up the
//memory for GC to handle
func (rf *FileHandle) Close() error {
	if rf.file == nil {
		return nil
	}
	fi := rf.file
	rf.file = nil

	return fi.Close()
}

//FilteredInDir accepts a directory string and a filter
//pattern, will return a slice of FileHandle
func FilteredInDir(dir string, filter string) (*[]FileHandle, error) {
	return findFiles(dir, filter)
}

//InDir accepts a dir string and returns all files
//in that directory
func InDir(dir string) (*[]FileHandle, error) {
	return findFiles(dir, "")
}

func findFiles(dir, filter string) (*[]FileHandle, error) {
	files := make([]FileHandle, 0)
	found, err := ioutil.ReadDir(dir)
	if err != nil {
		return nil, err
	}

	for _, stat := range found {
		if stat.IsDir() {
			continue
		}

		if filter != "" {
			reg, err := regexp.Compile(filter)
			if err != nil {
				return nil, err
			}

			//TODO: regex filtering
			matched := reg.MatchString(stat.Name())

			if !matched {
				continue
			}
		}

		files = append(files, FileHandle{
			Path: filepath.Join(dir, stat.Name()),
		})
	}

	return &files, nil
}
