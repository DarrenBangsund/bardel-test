package render

import (
	"math"
)

type report struct {
	total float64
	Avg   float64
	Min   float64
	Max   float64
}

func newReport() *report {
	return &report{
		total: 0,
		Avg:   0,
		Min:   -1.0,
		Max:   -1.0,
	}
}

func (r *report) updateMin(v float64) *report {
	if r.Min < 0 {
		r.Min = v
	} else {
		r.Min = math.Min(r.Min, v)
	}

	return r
}

func (r *report) updateMax(v float64) *report {
	if r.Max < 0 {
		r.Max = v
	} else {
		r.Max = math.Max(r.Max, v)
	}

	return r
}

type rendersReport struct {
	Total         int
	TotalReported int
	Time          *report
	RAM           *report
	CPU           *report
}

func newRendersReport() *rendersReport {
	return &rendersReport{
		Total:         0,
		TotalReported: 0,
		Time:          newReport(),
		RAM:           newReport(),
		CPU:           newReport(),
	}
}

//UpdateReport accepts a render{} type and will modify
//itself to reflect the incoming data
func (r *rendersReport) UpdateReport(render *render) {
	r.TotalReported++
	r.Time.total += render.RTime
	r.RAM.total += render.PkRAM
	r.CPU.total += render.PkCPU

	r.Time.Avg = r.Time.total / float64(r.TotalReported)
	r.RAM.Avg = r.RAM.total / float64(r.TotalReported)
	r.CPU.Avg = r.CPU.total / float64(r.TotalReported)

	////UPDATE MIN/MAX
	r.Time.updateMax(render.RTime).updateMin(render.RTime)
	r.RAM.updateMax(render.PkRAM).updateMin(render.PkRAM)
	r.CPU.updateMax(render.PkCPU).updateMin(render.PkCPU)
}
