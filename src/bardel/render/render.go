package render

import (
	"reflect"
	"regexp"
	"strconv"
)

type render struct {
	RUID     string
	RAName   string
	RName    string
	Frames   int
	RSuccess bool
	RTime    float64
	PkRAM    float64
	PkCPU    float64
}

func newRender(data []string) render {
	r := render{}

	for i, val := range data {
		//Here we do some dynamic type asserting of the incoming values.
		//
		//We can assume that the incoming values are always strings
		//but we might want to store those as something else, so
		//assert those types through reflection, the intention with
		//this is to create ease of maintaining later if we wanted to
		//add or remove fields, it only requires a change in the struct
		//definition and the incoming struct order.
		//
		//NOTE: ORDER MATTERS!!
		structFType := reflect.TypeOf(r).Field(i)
		structFValue := reflect.ValueOf(&r).Elem().Field(i)
		newVal := reflect.ValueOf(val)
		incValue := reflect.New(structFType.Type).Elem()

		switch structFType.Type.String() {
		case "string":
			newVal = newVal.Convert(structFType.Type)
			break
		case "int64":
			fallthrough
		case "int32":
			fallthrough
		case "int":
			in, _ := strconv.ParseInt(val, 10, 0)
			newVal = reflect.ValueOf(int(in))
			break
		case "bool":
			b, _ := strconv.ParseBool(val)
			newVal = reflect.ValueOf(b)
			break
		case "float32":
			f, _ := strconv.ParseFloat(val, 32)
			newVal = reflect.ValueOf(float32(f))
		case "float64":
			f, _ := strconv.ParseFloat(val, 64)
			newVal = reflect.ValueOf(float64(f))
		}

		incValue.Set(newVal)
		structFValue.Set(incValue)
	}

	return r
}

//Renders contains a list of type render and flags for configuration
type Renders struct {
	renders []render
	Config  *Config
}

//NewRenders initializes an empty flags config and returns itself
func NewRenders() *Renders {
	return &Renders{
		Config: NewConfig(),
	}
}

//AddRender accepts a data slice and appends a new render to itself
//see newRender()
func (r *Renders) AddRender(data []string) {
	r.renders = append(r.renders, newRender(data))
}

//GenerateReport will return returns a report of the current
//Renders{} struct
func (r *Renders) GenerateReport() (*rendersReport, error) {
	report := newRendersReport()

	//Compile the app string from config
	appReg, err := regexp.Compile(r.Config.app)
	if err != nil {
		return nil, err
	}

	//Compile the renderer string from config
	rendReg, err := regexp.Compile(r.Config.renderer)
	if err != nil {
		return nil, err
	}

	for _, v := range r.renders {
		report.Total++

		if !appReg.MatchString(v.RAName) {
			continue
		}

		if !rendReg.MatchString(v.RName) {
			continue
		}

		if !r.Config.failed && !v.RSuccess {
			continue
		}

		//if all tests are passed we can modify the report
		//update
		report.UpdateReport(&v)
	}

	return report, nil
}
