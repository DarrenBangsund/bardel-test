package render

type Config struct {
	app      string
	renderer string
	failed   bool
}

//NewConfig initializes an instance of Config{} and
//fills it with empty values
func NewConfig() *Config {
	return &Config{
		app:      "",
		renderer: "",
		failed:   false,
	}
}

//SetApp is a setter for the app field
func (f *Config) SetApp(str string) {
	f.app = str
}

//App is a getter for the app field
func (f *Config) App() string {
	return f.app
}

//SetRenderer is a setter for the renderer field
func (f *Config) SetRenderer(str string) {
	f.renderer = str
}

//Renderer is a getter for the renderer field
func (f *Config) Renderer() string {
	return f.renderer
}

//SetFailed is a setter for the failed field
func (f *Config) SetFailed(b bool) {
	f.failed = b
}

//Failed is a getter for the failed field
func (f *Config) Failed() bool {
	return f.failed
}
