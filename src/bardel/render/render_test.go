package render

import (
	"fmt"
	"io/ioutil"
	"math/rand"
	"os"
	"path/filepath"
	"strings"
	"testing"
	"time"
)

var wd, _ = os.Getwd()
var testRendersLoc = filepath.Join(wd, "..", "..", "..", "test-renders")

const (
	RUID     = "asdf"
	RAName   = "renderApplication"
	RName    = "rendererName"
	Frames   = "16000"
	RSuccess = "false"
)

type month int

const (
	January   month = 31
	February        = 28
	March           = 31
	April           = 30
	May             = 31
	June            = 30
	July            = 31
	August          = 31
	September       = 30
	October         = 31
	November        = 30
	December        = 31
)

func randomString(length int) string {
	out := ""
	for i := 0; i < length; i++ {
		rand.Seed(int64(time.Now().Nanosecond() * rand.Int()))
		out += fmt.Sprintf("%c", rand.Intn(25)+65)
	}
	return out
}

func generateRandomRenderString() string {
	r := rand.Intn(2) > 0
	rt := 0
	RAM := float32(0)
	CPU := float32(0)
	if r {
		rt = rand.Intn(500000)
		RAM = rand.Float32() * 100
		CPU = rand.Float32() * 100
	}

	RANames := []string{
		"renderApplicationName",
		"otherRenderApplicationName",
	}

	RNames := []string{
		"renderName",
		"otherRenderName",
	}

	if r {
		return fmt.Sprintf("%v,%v,%v,%v,%v,%v,%v,%v",
			randomString(16),
			RANames[rand.Intn(len(RANames))],
			RNames[rand.Intn(len(RNames))],
			rand.Intn(1000000),
			r,
			rt,
			RAM,
			CPU,
		)
	}

	return fmt.Sprintf("%v,%v,%v,%v,%v",
		randomString(16),
		RANames[rand.Intn(len(RANames))],
		RNames[rand.Intn(len(RNames))],
		rand.Intn(1000000),
		r,
	)

}

func TestGenerateTestRenderFiles(t *testing.T) {
	mos := []month{
		January,
		February,
		March,
		April,
		May,
		June,
		July,
		August,
		September,
		October,
		November,
		December,
	}

	for i := 0; i < 500; i++ {

		yr := rand.Intn(19) + 2000
		mo := rand.Intn(12)
		da := rand.Intn(int(mos[mo]))
		newname := fmt.Sprintf("renders_%v-%v-%v.csv", yr, mo, da)

		b := ""
		for j := 0; j < 500; j++ {
			b = fmt.Sprintf("%v%v%v", b, "\n", generateRandomRenderString())
		}
		// t.Log()
		ioutil.WriteFile(filepath.Join(testRendersLoc, newname), []byte(b), os.ModePerm)
	}
}

func TestAddNewRender(t *testing.T) {
	r := &Renders{}
	for i := 0; i < 1000; i++ {
		r.AddRender(strings.Split(generateRandomRenderString(), ","))
	}

	for _, v := range r.renders {
		t.Log(v)
	}

}

func TestNewRender(t *testing.T) {
	for i := 0; i < 10000; i++ {
		newRender([]string{
			RUID,
			RAName,
			RName,
			Frames,
			RSuccess,
		})
	}
}

func TestReport(t *testing.T) {
	r := NewRenders()
	for i := 0; i < 5000; i++ {
		r.AddRender(strings.Split(generateRandomRenderString(), ","))
	}

	report, _ := r.GenerateReport()
	////OUTPUT
	//TIME
	t.Log("-----TIME-----")
	t.Logf("avg: %v", report.Time.Avg)
	t.Logf("max: %v", report.Time.Max)
	t.Logf("min: %v", report.Time.Min)
	//RAM
	t.Log("-----RAM-----")
	t.Logf("avg: %v", report.RAM.Avg)
	t.Logf("max: %v", report.RAM.Max)
	t.Logf("min: %v", report.RAM.Min)
	//CPU
	t.Log("-----CPU-----")
	t.Logf("avg: %v", report.CPU.Avg)
	t.Logf("max: %v", report.CPU.Max)
	t.Logf("min: %v", report.CPU.Min)

	t.Logf("%v out of %v reported", report.TotalReported, report.Total)
}
