GOPATH=$(PWD)
export GOPATH

GOCMD=go
GOBUILD=$(GOCMD) build
BINARY="BARDEL-TEST"


all: build
build:
	$(GOBUILD) -o $(BINARY)

clean:
	rm -rf ./pkg
	rm -rf $(BINARY)