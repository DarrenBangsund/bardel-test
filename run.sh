binary="BARDEL-TEST"
args="$@"

if [ -f "./$binary" ]
then
	./$binary $args
else
  echo "Binary not found! Try running 'make' first"
fi